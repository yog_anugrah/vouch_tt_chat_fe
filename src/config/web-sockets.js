import io from 'socket.io-client';

const STRAPI_ENDPOINT = 'https://vouch-tt-api.herokuapp.com/';


// ========= check if on prod or dev ==========
// let STRAPI_ENDPOINT;
// if (process.env.NODE_ENV !== 'production') {
//     STRAPI_ENDPOINT = 'http://localhost:1337';
// } else {
//     STRAPI_ENDPOINT = process.env.REACT_APP_SERVER_URL
// }


export const socket = io(STRAPI_ENDPOINT);
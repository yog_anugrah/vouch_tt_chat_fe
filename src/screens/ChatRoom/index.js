import React, { useEffect, useState } from 'react';
import { Input } from 'antd';
import Header from '../../components/Header';
import Messages from '../../components/Messages';
import List from '../../components/List';
import { socket } from '../../config/web-sockets';
import { history } from '../../config/network';
import {
    ChatContainer,
    StyledContainer,
    ChatBox,
    StyledButton,
    SendIcon
} from './styles';

function ChatRoom(props) {
    const {username, room, joinData } = props;
    const [messages, setMessages] = useState([]);
    const [users, setUsers] = useState([]);
    // const [draftText, setDraftText] = useState('')
    const [message, setMessage] = useState('')
    useEffect(() => {
        // socket.on("roomInfo", (users) => {
        //     setUsers(users);
        // });
        if( Object.keys(joinData).length > 0) {
            console.log(messages, "Message Before Join Data")
            setMessages([joinData])
            console.log(messages, "Message After Join Data")
            socket.on('message', (message, error) => {
                // message.map(pesan => {
                //     setMessages(msgs => [ ...msgs, pesan ]);
                // })
                setMessages(message)
                // setMessages(msgs => [ ...msgs, message ]);
                console.log(messages, "Message On Socket")
            });
        } 
        else {
            history.push('/join')
        }
     }, [joinData])

    const handleChange = (e) => {
        setMessage(e.target.value);
    }
      
    const handleClick = (e) => {
        sendMessage(message);
    }

    const sendMessage = (messages) => {
        if(messages) {
            socket.emit('sendMessage',{ userId: joinData.userData.id, message: messages }, (error) => {
                if(error) {
                    alert(error)
                    history.push('/join');
                }
            });
            setMessage('')
        } else {
            alert("Message can't be empty")
        }
    }
    return (
        <>
        <ChatContainer>
            <Header room={room} />
            <StyledContainer>
                <ChatBox>
                    <Messages 
                        messages={messages} 
                        username={username}
                    />
                    <Input
                        type="text"
                        placeholder="Type your message"
                        // value={draftText}
                        value={message}
                        onChange={handleChange}
                    />
                    <StyledButton
                        onClick={handleClick}
                    >
                        <SendIcon>
                            <i className="fa fa-paper-plane" />
                        </SendIcon>
                    </StyledButton>
                </ChatBox>      
            </StyledContainer>
        </ChatContainer>
        </>
        
    );
};

export default ChatRoom;
